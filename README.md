# OpenML dataset: BitInfoCharts-wo-tweets-preprocessed

https://www.openml.org/d/46122

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bitcoin data scrapped from BitInfoCharts, without 'tweets' and with preprocessing.

Several Bitcoin related data scrapped directly from BitInfoCharts. 'date' in the format %Y-%m-%d.
The 'tweets' column was dropped due to too many nan values (values only between 2014-04-09 and 2023-03-14).
Besides, we have only kept the rows between the max(dates with non NaN values of each column) and min(dates with non NaN values of each column), which
leave us with dates between 2011-04-14 and 2024-05-26.

Preprocessing:

1 - Renamed columns to 'date' and 'value_X' with X from 0 to 17 (number of columns of original dataset).

2 - Created columns 'time_step' and 'id_series'. There is only one 'id_series' (0).

3 - Filled nan values by propagating the last valid observation to next valid (ffill).

The columns with some missing values were:
'median_transaction_fee': 'value_9'
'confirmationtime': 'value_10'
'activeaddresses': 'value_15'
'top100cap': 'value_16'

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46122) of an [OpenML dataset](https://www.openml.org/d/46122). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46122/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46122/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46122/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

